import copy
import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-scaling-target-" + str(int(time.time())),
    "service_namespace": "rds",
    "scalable_dimension": "rds:cluster:ReadReplicaCount",
    "min_capacity": 1,
    "max_capacity": 2,
    "suspended_state": {
        "DynamicScalingInSuspended": False,
        "DynamicScalingOutSuspended": False,
        "ScheduledScalingSuspended": False,
    },
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling target creation is not supported in localstack.",
)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_rds_db_cluster, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER[
        "scaling_resource_id"
    ] = f"cluster:{aws_rds_db_cluster.get('resource_id')}"

    ret = await hub.states.aws.application_autoscaling.scalable_target.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.application_autoscaling.scalable_target",
                name=PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.application_autoscaling.scalable_target",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_scaling_target(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling target creation is not supported in localstack.",
)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = (
        await hub.states.aws.application_autoscaling.scalable_target.describe(ctx)
    )
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert (
        "aws.application_autoscaling.scalable_target.present"
        in describe_ret[resource_id]
    )
    described_resource = describe_ret[resource_id].get(
        "aws.application_autoscaling.scalable_target.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_scaling_target(described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling target creation is not supported in localstack.",
)
@pytest.mark.dependency(name="modify_capacity", depends=["describe"])
async def test_modify_capacity(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["min_capacity"] = 2
    new_parameter["max_capacity"] = 5
    ret = await hub.states.aws.application_autoscaling.scalable_target.present(
        ctx, **new_parameter
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.application_autoscaling.scalable_target",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.application_autoscaling.scalable_target",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert_scaling_target(ret["old_state"], PARAMETER)
    assert_scaling_target(ret["new_state"], new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling target creation is not supported in localstack.",
)
@pytest.mark.dependency(name="modify_suspended_state", depends=["modify_capacity"])
async def test_modify_suspended_state(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["suspended_state"] = {
        "DynamicScalingInSuspended": True,
        "DynamicScalingOutSuspended": True,
        "ScheduledScalingSuspended": True,
    }
    ret = await hub.states.aws.application_autoscaling.scalable_target.present(
        ctx, **new_parameter
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.application_autoscaling.scalable_target",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.application_autoscaling.scalable_target",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert_scaling_target(ret["old_state"], PARAMETER)
    assert_scaling_target(ret["new_state"], new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling target creation is not supported in localstack.",
)
@pytest.mark.dependency(name="absent", depends=["modify_suspended_state"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.application_autoscaling.scalable_target.absent(
        ctx,
        name=PARAMETER["name"],
        scaling_resource_id=PARAMETER["scaling_resource_id"],
        service_namespace=PARAMETER["service_namespace"],
        scalable_dimension=PARAMETER["scalable_dimension"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert_scaling_target(ret["old_state"], PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.application_autoscaling.scalable_target",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.application_autoscaling.scalable_target",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling target creation is not supported in localstack.",
)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.application_autoscaling.scalable_target.absent(
        ctx,
        name=PARAMETER["name"],
        scaling_resource_id=PARAMETER["scaling_resource_id"],
        service_namespace=PARAMETER["service_namespace"],
        scalable_dimension=PARAMETER["scalable_dimension"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.application_autoscaling.scalable_target",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "skipping test in localstack as scaling target creation is not supported in localstack.",
)
async def test_scaling_target_absent_with_none_resource_id(hub, ctx):
    scaling_target_temp_name = "idem-test-scaling_target-" + str(int(time.time()))
    # Delete scaling_target with resource_id as None. Result in no-op.
    ret = await hub.states.aws.application_autoscaling.scalable_target.absent(
        ctx,
        name=scaling_target_temp_name,
        scaling_resource_id=None,
        service_namespace=PARAMETER["service_namespace"],
        scalable_dimension=PARAMETER["scalable_dimension"],
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.application_autoscaling.scalable_target",
            name=scaling_target_temp_name,
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    # skipping test in localstack as scaling target creation is not supported in localstack.
    if hub.tool.utils.is_running_localstack(ctx):
        return
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.application_autoscaling.scalable_target.absent(
            ctx,
            name=PARAMETER["name"],
            scaling_resource_id=PARAMETER["scaling_resource_id"],
            service_namespace=PARAMETER["service_namespace"],
            scalable_dimension=PARAMETER["scalable_dimension"],
            resource_id=None,
        )
        assert ret["result"], ret["comment"]


def assert_scaling_target(resource, parameters):
    assert parameters.get("service_namespace") == resource.get("service_namespace")
    assert parameters.get("scaling_resource_id") == resource.get("scaling_resource_id")
    assert parameters.get("scalable_dimension") == resource.get("scalable_dimension")
    assert parameters.get("min_capacity") == resource.get("min_capacity")
    assert parameters.get("enabled") == resource.get("enabled")
    assert parameters.get("max_capacity") == resource.get("max_capacity")
    assert parameters.get("suspended_state") == resource.get("suspended_state")
