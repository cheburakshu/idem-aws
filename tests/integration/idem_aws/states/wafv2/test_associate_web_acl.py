import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_web_acl_association(hub, ctx, aws_wafv2_web_acl, aws_api_gateway):
    # Skipping this as web_acl association is not supported in localstack and only working with real aws.
    if hub.tool.utils.is_running_localstack(ctx):
        return

    associate_web_acl_temp_name = "idem-test-associate-web-acl-" + str(uuid.uuid4())
    web_acl_arn = aws_wafv2_web_acl.get("web_acl_arn")
    rest_api_id = aws_api_gateway["ret"]["id"]
    resource_arn = hub.tool.aws.arn_utils.build(
        service="apigateway",
        region="us-west-2",
        resource="/restapis/" + rest_api_id + "/stages/prod",
    )

    # Associates a web ACL with a regional application resource with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.wafv2.associate_web_acl.present(
        test_ctx,
        name=associate_web_acl_temp_name,
        web_acl_arn=web_acl_arn,
        resource_arn=resource_arn,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert (
        f"Would create aws.waf.web_acl_association '{associate_web_acl_temp_name}'"
        in ret["comment"]
    )
    assert associate_web_acl_temp_name == resource.get("name")
    assert web_acl_arn == resource.get("web_acl_arn")
    assert resource_arn == resource.get("resource_arn")

    # Associates a web ACL with a regional application resource with real aws
    ret = await hub.states.aws.wafv2.associate_web_acl.present(
        ctx,
        name=associate_web_acl_temp_name,
        web_acl_arn=web_acl_arn,
        resource_arn=resource_arn,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert (
        f"Created aws.waf.web_acl_association '{associate_web_acl_temp_name}'"
        in ret["comment"]
    )
    assert associate_web_acl_temp_name == resource.get("name")
    assert web_acl_arn == resource.get("web_acl_arn")
    assert resource_arn == resource.get("resource_arn")

    # Describe web ACL association
    describe_ret = await hub.states.aws.wafv2.associate_web_acl.describe(
        ctx,
    )
    assert resource_id in describe_ret
    assert "aws.wafv2.associate_web_acl.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.wafv2.associate_web_acl.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert web_acl_arn == described_resource_map.get("web_acl_arn")
    assert resource_arn == described_resource_map.get("resource_arn")

    # Associates again
    ret = await hub.states.aws.wafv2.associate_web_acl.present(
        ctx,
        name=associate_web_acl_temp_name,
        web_acl_arn=web_acl_arn,
        resource_arn=resource_arn,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource_old_state = ret.get("old_state")
    resource_new_state = ret.get("new_state")
    assert web_acl_arn == resource_old_state["web_acl_arn"]
    assert resource_arn == resource_old_state["resource_arn"]
    assert associate_web_acl_temp_name == resource_old_state["name"]
    assert web_acl_arn == resource_new_state["web_acl_arn"]
    assert resource_arn == resource_new_state["resource_arn"]
    assert associate_web_acl_temp_name == resource_new_state["name"]
    assert (
        f"aws.waf.web_acl_association '{associate_web_acl_temp_name}' already exists"
        in ret["comment"]
    )

    # Disassociates a web ACL with a regional application resource with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.wafv2.associate_web_acl.absent(
        test_ctx,
        name=associate_web_acl_temp_name,
        resource_id=resource_arn,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource_old_state = ret.get("old_state")
    assert resource_arn == resource_old_state["resource_arn"]
    assert associate_web_acl_temp_name == resource_old_state["name"]
    assert resource_arn == resource_old_state["resource_arn"]
    assert (
        f"Would disassociate aws.waf.web_acl_association '{associate_web_acl_temp_name}'"
        in ret["comment"]
    )

    # Disassociates a web ACL with a regional application resource with real aws
    ret = await hub.states.aws.wafv2.associate_web_acl.absent(
        ctx,
        name=associate_web_acl_temp_name,
        resource_id=resource_arn,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource_old_state = ret.get("old_state")
    assert resource_arn == resource_old_state["resource_arn"]
    assert associate_web_acl_temp_name == resource_old_state["name"]
    assert resource_arn == resource_old_state["resource_arn"]
    assert (
        f"Deleted aws.waf.web_acl_association '{associate_web_acl_temp_name}'"
        in ret["comment"]
    )

    # Disassociate again
    ret = await hub.states.aws.wafv2.associate_web_acl.absent(
        ctx,
        name=associate_web_acl_temp_name,
        resource_id=resource_arn,
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"aws.wafv2.associate_web_acl '{associate_web_acl_temp_name}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_association_absent_with_none_resource_id(hub, ctx):
    associate_web_acl_temp_name = "idem-test-associate-web-acl-" + str(uuid.uuid4())
    # Delete association with resource_id as None. Result in no-op.
    ret = await hub.states.aws.wafv2.associate_web_acl.absent(
        ctx, name=associate_web_acl_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        f"aws.wafv2.associate_web_acl '{associate_web_acl_temp_name}' already absent"
        in ret["comment"]
    )
