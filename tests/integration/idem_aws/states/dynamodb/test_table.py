import copy
import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-dynamodb-table-" + str(int(time.time())),
    "attribute_definitions": [
        {
            "AttributeName": "Artist",
            "AttributeType": "S",
        },
        {
            "AttributeName": "SongTitle",
            "AttributeType": "S",
        },
    ],
    "key_schema": [
        {
            "AttributeName": "Artist",
            "KeyType": "HASH",
        },
        {
            "AttributeName": "SongTitle",
            "KeyType": "RANGE",
        },
    ],
    "billing_mode": "PROVISIONED",
    "point_in_time_recovery": {
        "Enabled": False,
    },
    "provisioned_throughput": {
        "ReadCapacityUnits": 123,
        "WriteCapacityUnits": 123,
    },
    "sse_specification": {
        "Enabled": False,
    },
    "stream_specification": {
        "StreamEnabled": True,
        "StreamViewType": "NEW_IMAGE",
    },
    "time_to_live": {
        "Enabled": False,
    },
}


# Some calls used during get (ie "describe_continuous_backups" and "describe_time_to_live")
# are not supported by localstack, therefore most tests are being skipped in localstack and
# only tested in real AWS


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}

    ret = await hub.states.aws.dynamodb.table.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]

    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["attribute_definitions"] == resource.get("attribute_definitions")
    assert PARAMETER["key_schema"] == resource.get("key_schema")
    assert PARAMETER["billing_mode"] == resource.get("billing_mode")
    assert PARAMETER["point_in_time_recovery"] == resource.get("point_in_time_recovery")
    assert PARAMETER["provisioned_throughput"] == resource.get("provisioned_throughput")
    assert PARAMETER["sse_specification"] == resource.get("sse_specification")
    assert PARAMETER["stream_specification"] == resource.get("stream_specification")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["time_to_live"] == resource.get("time_to_live")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get", depends=["present"])
# This test is here to avoid the need to create a table fixture for exec.get() testing
async def test_get(hub, ctx):
    ret = await hub.exec.aws.dynamodb.table.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["attribute_definitions"] == resource.get("attribute_definitions")
    assert PARAMETER["key_schema"] == resource.get("key_schema")
    assert PARAMETER["billing_mode"] == resource.get("billing_mode")
    assert PARAMETER["point_in_time_recovery"] == resource.get("point_in_time_recovery")
    assert PARAMETER["provisioned_throughput"] == resource.get("provisioned_throughput")
    assert PARAMETER["sse_specification"] == resource.get("sse_specification")
    assert PARAMETER["stream_specification"] == resource.get("stream_specification")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["time_to_live"] == resource.get("time_to_live")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["exec-get"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.dynamodb.table.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret

    assert "aws.dynamodb.table.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get("aws.dynamodb.table.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["name"] == described_resource_map.get("name")
    assert PARAMETER["attribute_definitions"] == described_resource_map.get(
        "attribute_definitions"
    )
    assert PARAMETER["key_schema"] == described_resource_map.get("key_schema")
    assert PARAMETER["billing_mode"] == described_resource_map.get("billing_mode")
    assert PARAMETER["point_in_time_recovery"] == described_resource_map.get(
        "point_in_time_recovery"
    )
    assert PARAMETER["provisioned_throughput"] == described_resource_map.get(
        "provisioned_throughput"
    )
    assert PARAMETER["sse_specification"] == described_resource_map.get(
        "sse_specification"
    )
    assert PARAMETER["stream_specification"] == described_resource_map.get(
        "stream_specification"
    )
    assert PARAMETER["tags"] == described_resource_map.get("tags")
    assert PARAMETER["time_to_live"] == described_resource_map.get("time_to_live")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update_attributes", depends=["describe"])
async def test_update_attributes(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["provisioned_throughput"] = {
        "ReadCapacityUnits": 131,
        "WriteCapacityUnits": 131,
    }

    ret = await hub.states.aws.dynamodb.table.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["attribute_definitions"] == old_resource.get(
        "attribute_definitions"
    )
    assert PARAMETER["key_schema"] == old_resource.get("key_schema")
    assert PARAMETER["billing_mode"] == old_resource.get("billing_mode")
    assert PARAMETER["point_in_time_recovery"] == old_resource.get(
        "point_in_time_recovery"
    )
    assert PARAMETER["provisioned_throughput"] == old_resource.get(
        "provisioned_throughput"
    )
    assert PARAMETER["sse_specification"] == old_resource.get("sse_specification")
    assert PARAMETER["stream_specification"] == old_resource.get("stream_specification")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["time_to_live"] == old_resource.get("time_to_live")

    resource = ret["new_state"]
    assert new_parameter["name"] == resource["name"]
    assert new_parameter["resource_id"] == resource["resource_id"]
    assert new_parameter["attribute_definitions"] == resource.get(
        "attribute_definitions"
    )
    assert new_parameter["key_schema"] == resource.get("key_schema")
    assert new_parameter["billing_mode"] == resource.get("billing_mode")
    assert new_parameter["point_in_time_recovery"] == resource.get(
        "point_in_time_recovery"
    )
    assert new_parameter["provisioned_throughput"] == resource.get(
        "provisioned_throughput"
    )
    assert new_parameter["sse_specification"] == resource.get("sse_specification")
    assert new_parameter["stream_specification"] == resource.get("stream_specification")
    assert new_parameter["tags"] == resource.get("tags")
    assert new_parameter["time_to_live"] == resource.get("time_to_live")

    if __test:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.would_update_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.update_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(
    name="update_point_in_time_recovery", depends=["update_attributes"]
)
async def test_update_point_in_time_recovery(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["point_in_time_recovery"] = {"Enabled": True}

    ret = await hub.states.aws.dynamodb.table.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["attribute_definitions"] == old_resource.get(
        "attribute_definitions"
    )
    assert PARAMETER["key_schema"] == old_resource.get("key_schema")
    assert PARAMETER["billing_mode"] == old_resource.get("billing_mode")
    assert PARAMETER["point_in_time_recovery"] == old_resource.get(
        "point_in_time_recovery"
    )
    assert PARAMETER["provisioned_throughput"] == old_resource.get(
        "provisioned_throughput"
    )
    assert PARAMETER["sse_specification"] == old_resource.get("sse_specification")
    assert PARAMETER["stream_specification"] == old_resource.get("stream_specification")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["time_to_live"] == old_resource.get("time_to_live")

    resource = ret["new_state"]
    assert new_parameter["name"] == resource["name"]
    assert new_parameter["resource_id"] == resource["resource_id"]
    assert new_parameter["attribute_definitions"] == resource.get(
        "attribute_definitions"
    )
    assert new_parameter["key_schema"] == resource.get("key_schema")
    assert new_parameter["billing_mode"] == resource.get("billing_mode")
    assert new_parameter["point_in_time_recovery"] == resource.get(
        "point_in_time_recovery"
    )
    assert new_parameter["provisioned_throughput"] == resource.get(
        "provisioned_throughput"
    )
    assert new_parameter["sse_specification"] == resource.get("sse_specification")
    assert new_parameter["stream_specification"] == resource.get("stream_specification")
    assert new_parameter["tags"] == resource.get("tags")
    assert new_parameter["time_to_live"] == resource.get("time_to_live")

    if __test:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.would_update_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.update_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(
    name="update_time_to_live", depends=["update_point_in_time_recovery"]
)
async def test_update_time_to_live(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["time_to_live"] = {"Enabled": True, "AttributeName": "ttl"}

    ret = await hub.states.aws.dynamodb.table.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["attribute_definitions"] == old_resource.get(
        "attribute_definitions"
    )
    assert PARAMETER["key_schema"] == old_resource.get("key_schema")
    assert PARAMETER["billing_mode"] == old_resource.get("billing_mode")
    assert PARAMETER["point_in_time_recovery"] == old_resource.get(
        "point_in_time_recovery"
    )
    assert PARAMETER["provisioned_throughput"] == old_resource.get(
        "provisioned_throughput"
    )
    assert PARAMETER["sse_specification"] == old_resource.get("sse_specification")
    assert PARAMETER["stream_specification"] == old_resource.get("stream_specification")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["time_to_live"] == old_resource.get("time_to_live")

    resource = ret["new_state"]
    assert new_parameter["name"] == resource["name"]
    assert new_parameter["resource_id"] == resource["resource_id"]
    assert new_parameter["attribute_definitions"] == resource.get(
        "attribute_definitions"
    )
    assert new_parameter["key_schema"] == resource.get("key_schema")
    assert new_parameter["billing_mode"] == resource.get("billing_mode")
    assert new_parameter["point_in_time_recovery"] == resource.get(
        "point_in_time_recovery"
    )
    assert new_parameter["provisioned_throughput"] == resource.get(
        "provisioned_throughput"
    )
    assert new_parameter["sse_specification"] == resource.get("sse_specification")
    assert new_parameter["stream_specification"] == resource.get("stream_specification")
    assert new_parameter["tags"] == resource.get("tags")
    assert new_parameter["time_to_live"] == resource.get("time_to_live")

    if __test:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.would_update_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.update_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="add_tags", depends=["update_time_to_live"])
async def test_add_tags(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["tags"].update(
        {
            "New-Tag": f"idem-test-value-{str(int(time.time()))}",
        }
    )

    ret = await hub.states.aws.dynamodb.table.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["attribute_definitions"] == old_resource.get(
        "attribute_definitions"
    )
    assert PARAMETER["key_schema"] == old_resource.get("key_schema")
    assert PARAMETER["billing_mode"] == old_resource.get("billing_mode")
    assert PARAMETER["point_in_time_recovery"] == old_resource.get(
        "point_in_time_recovery"
    )
    assert PARAMETER["provisioned_throughput"] == old_resource.get(
        "provisioned_throughput"
    )
    assert PARAMETER["sse_specification"] == old_resource.get("sse_specification")
    assert PARAMETER["stream_specification"] == old_resource.get("stream_specification")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["time_to_live"] == old_resource.get("time_to_live")

    resource = ret["new_state"]
    assert new_parameter["name"] == resource["name"]
    assert new_parameter["resource_id"] == resource["resource_id"]
    assert new_parameter["attribute_definitions"] == resource.get(
        "attribute_definitions"
    )
    assert new_parameter["key_schema"] == resource.get("key_schema")
    assert new_parameter["billing_mode"] == resource.get("billing_mode")
    assert new_parameter["point_in_time_recovery"] == resource.get(
        "point_in_time_recovery"
    )
    assert new_parameter["provisioned_throughput"] == resource.get(
        "provisioned_throughput"
    )
    assert new_parameter["sse_specification"] == resource.get("sse_specification")
    assert new_parameter["stream_specification"] == resource.get("stream_specification")
    assert new_parameter["tags"] == resource.get("tags")
    assert new_parameter["time_to_live"] == resource.get("time_to_live")

    added = {
        tag: new_parameter["tags"][tag]
        for tag in new_parameter["tags"]
        if tag not in PARAMETER["tags"]
    }
    removed = {
        tag: PARAMETER["tags"][tag]
        for tag in PARAMETER["tags"]
        if tag not in new_parameter["tags"]
    }
    if __test:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.would_update_tags_comment(removed, added)
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.update_tags_comment(removed, added)
            == ret["comment"]
        )
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="remove_tags", depends=["add_tags"])
async def test_remove_tags(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    new_parameter = copy.deepcopy(PARAMETER)
    del new_parameter["tags"]["New-Tag"]

    ret = await hub.states.aws.dynamodb.table.present(ctx, **new_parameter)

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["attribute_definitions"] == old_resource.get(
        "attribute_definitions"
    )
    assert PARAMETER["key_schema"] == old_resource.get("key_schema")
    assert PARAMETER["billing_mode"] == old_resource.get("billing_mode")
    assert PARAMETER["point_in_time_recovery"] == old_resource.get(
        "point_in_time_recovery"
    )
    assert PARAMETER["provisioned_throughput"] == old_resource.get(
        "provisioned_throughput"
    )
    assert PARAMETER["sse_specification"] == old_resource.get("sse_specification")
    assert PARAMETER["stream_specification"] == old_resource.get("stream_specification")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["time_to_live"] == old_resource.get("time_to_live")

    resource = ret["new_state"]
    assert new_parameter["name"] == resource["name"]
    assert new_parameter["resource_id"] == resource["resource_id"]
    assert new_parameter["attribute_definitions"] == resource.get(
        "attribute_definitions"
    )
    assert new_parameter["key_schema"] == resource.get("key_schema")
    assert new_parameter["billing_mode"] == resource.get("billing_mode")
    assert new_parameter["point_in_time_recovery"] == resource.get(
        "point_in_time_recovery"
    )
    assert new_parameter["provisioned_throughput"] == resource.get(
        "provisioned_throughput"
    )
    assert new_parameter["sse_specification"] == resource.get("sse_specification")
    assert new_parameter["stream_specification"] == resource.get("stream_specification")
    assert new_parameter["tags"] == resource.get("tags")
    assert new_parameter["time_to_live"] == resource.get("time_to_live")

    added = {
        tag: new_parameter["tags"][tag]
        for tag in new_parameter["tags"]
        if tag not in PARAMETER["tags"]
    }
    removed = {
        tag: PARAMETER["tags"][tag]
        for tag in PARAMETER["tags"]
        if tag not in new_parameter["tags"]
    }
    if __test:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.would_update_tags_comment(removed, added)
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.update_tags_comment(removed, added)
            == ret["comment"]
        )

    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["remove_tags"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test

    ret = await hub.states.aws.dynamodb.table.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                "aws.dynamodb.table",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    assert ret["old_state"] and not ret["new_state"]

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["attribute_definitions"] == old_resource.get(
        "attribute_definitions"
    )
    assert PARAMETER["key_schema"] == old_resource.get("key_schema")
    assert PARAMETER["billing_mode"] == old_resource.get("billing_mode")
    assert PARAMETER["point_in_time_recovery"] == old_resource.get(
        "point_in_time_recovery"
    )
    assert PARAMETER["provisioned_throughput"] == old_resource.get(
        "provisioned_throughput"
    )
    assert PARAMETER["sse_specification"] == old_resource.get("sse_specification")
    assert PARAMETER["stream_specification"] == old_resource.get("stream_specification")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["time_to_live"] == old_resource.get("time_to_live")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test

    ret = await hub.states.aws.dynamodb.table.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            "aws.dynamodb.table",
            PARAMETER["name"],
        )
        == ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_table_absent_with_none_resource_id(hub, ctx):
    table_temp_name = "idem-test-dynamodb-table-" + str(int(time.time()))

    ret = await hub.states.aws.dynamodb.table.absent(
        ctx, name=table_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            "aws.dynamodb.table",
            table_temp_name,
        )
        == ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])


# This cleanup fixture cleans up the resource after all tests have run
@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER

    yield None

    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.dynamodb.table.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
