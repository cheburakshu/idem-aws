import copy
import time
from collections import ChainMap

import pytest


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-authorizer-" + str(int(time.time())),
    "authorizer_type": "REQUEST",
    "identity_source": ["route.request.header.Auth"],
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
async def test_present(
    hub, ctx, __test, aws_apigatewayv2_api, aws_lambda_function, cleanup
):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["api_id"] = aws_apigatewayv2_api["api_id"]
    PARAMETER["authorizer_uri"] = hub.tool.aws.arn_utils.build(
        service="apigateway",
        region=ctx["acct"]["region_name"],
        account_id="lambda",
        resource="path/2015-03-31/functions/"
        + aws_lambda_function["function_arn"]
        + "/invocations",
    )

    ret = await hub.states.aws.apigatewayv2.authorizer.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            f"Would create aws.apigatewayv2.authorizer '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            f"Created aws.apigatewayv2.authorizer '{PARAMETER['name']}'"
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["api_id"] == resource.get("api_id")
    assert PARAMETER["authorizer_type"] == resource.get("authorizer_type")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["authorizer_uri"] == resource.get("authorizer_uri")
    assert PARAMETER["identity_source"] == resource.get("identity_source")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.apigatewayv2.authorizer.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.apigatewayv2.authorizer.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.apigatewayv2.authorizer.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["name"] == described_resource_map.get("name")
    assert PARAMETER["authorizer_type"] == described_resource_map.get("authorizer_type")
    assert PARAMETER["authorizer_uri"] == described_resource_map.get("authorizer_uri")
    assert PARAMETER["identity_source"] == described_resource_map.get("identity_source")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="exec-get", depends=["present"])
# This test is here to avoid the need to create an authorizer fixture for exec.get() testing
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.apigatewayv2.authorizer.get(
        ctx=ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        api_id=PARAMETER["api_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["authorizer_type"] == resource.get("authorizer_type")
    assert PARAMETER["authorizer_uri"] == resource.get("authorizer_uri")
    assert PARAMETER["identity_source"] == resource.get("identity_source")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="update_identity_source", depends=["describe"])
async def test_update_identity_source(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["identity_source"] = ["route.request.querystring.Name"]
    ret = await hub.states.aws.apigatewayv2.authorizer.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["authorizer_type"] == old_resource.get("authorizer_type")
    assert PARAMETER["authorizer_uri"] == old_resource.get("authorizer_uri")
    assert PARAMETER["identity_source"] == old_resource.get("identity_source")
    resource = ret["new_state"]
    assert new_parameter["name"] == resource.get("name")
    assert new_parameter["authorizer_type"] == resource.get("authorizer_type")
    assert new_parameter["authorizer_uri"] == resource.get("authorizer_uri")
    assert new_parameter["identity_source"] == resource.get("identity_source")
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="absent", depends=["update_identity_source"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigatewayv2.authorizer.absent(
        ctx,
        name=PARAMETER["name"],
        api_id=PARAMETER["api_id"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["authorizer_type"] == old_resource.get("authorizer_type")
    assert PARAMETER["authorizer_uri"] == old_resource.get("authorizer_uri")
    assert PARAMETER["identity_source"] == old_resource.get("identity_source")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.apigatewayv2.authorizer", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.apigatewayv2.authorizer", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigatewayv2.authorizer.absent(
        ctx,
        name=PARAMETER["name"],
        api_id=PARAMETER["api_id"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.authorizer", name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["present"])
async def test_absent_with_none_resource_id(hub, ctx):
    # Delete apigatewayv2 authorizer with no resource_id will consider it as absent
    ret = await hub.states.aws.apigatewayv2.authorizer.absent(
        ctx, name=PARAMETER["name"], resource_id=None, api_id=PARAMETER["api_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.authorizer", name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.apigatewayv2.authorizer.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
            api_id=PARAMETER["api_id"],
        )
        assert ret["result"], ret["comment"]
