import time
from collections import ChainMap
from typing import Any
from typing import Dict
from typing import List

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-target-group" + str(int(time.time())),
    "protocol_version": "HTTP1",
    "health_check_enabled": True,
    "health_check_path": "/health-check",
    "health_check_interval_seconds": 90,
    "health_check_timeout_seconds": 60,
    "healthy_threshold_count": 5,
    "unhealthy_threshold_count": 5,
    "matcher": {
        "HttpCode": "200",
    },
    "target_type": "lambda",
    "ip_address_type": "ipv4",
}
RESOURCE_TYPE = "aws.elbv2.target_group"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(
    hub,
    ctx,
    __test,
    aws_lambda_function,
    aws_lambda_elb_target_group_permissions,
    cleanup,
):
    assert aws_lambda_function and aws_lambda_elb_target_group_permissions
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["targets"] = [
        {"Id": aws_lambda_function.get("function_arn"), "AvailabilityZone": "all"}
    ]
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}
    attributes = [
        {
            "Key": "lambda.multi_value_headers.enabled",
            "Value": "false",
        },
    ]
    PARAMETER["attributes"] = attributes
    ret = await hub.states.aws.elbv2.target_group.present(
        ctx,
        name=PARAMETER["name"],
        protocol_version=PARAMETER["protocol_version"],
        health_check_enabled=PARAMETER["health_check_enabled"],
        health_check_path=PARAMETER["health_check_path"],
        health_check_interval_seconds=PARAMETER["health_check_interval_seconds"],
        health_check_timeout_seconds=PARAMETER["health_check_timeout_seconds"],
        healthy_threshold_count=PARAMETER["healthy_threshold_count"],
        unhealthy_threshold_count=PARAMETER["unhealthy_threshold_count"],
        matcher=PARAMETER["matcher"],
        target_type=PARAMETER["target_type"],
        ip_address_type=PARAMETER["ip_address_type"],
        tags=PARAMETER["tags"],
        targets=PARAMETER["targets"],
        attributes=PARAMETER["attributes"],
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["health_check_path"] == resource.get("health_check_path")
    assert PARAMETER["health_check_enabled"] == resource.get("health_check_enabled")
    assert PARAMETER["health_check_interval_seconds"] == resource.get(
        "health_check_interval_seconds"
    )
    assert PARAMETER["health_check_timeout_seconds"] == resource.get(
        "health_check_timeout_seconds"
    )
    assert PARAMETER["healthy_threshold_count"] == resource.get(
        "healthy_threshold_count"
    )
    assert PARAMETER["unhealthy_threshold_count"] == resource.get(
        "unhealthy_threshold_count"
    )
    assert PARAMETER["matcher"] == resource.get("matcher")
    assert PARAMETER["target_type"] == resource.get("target_type")
    assert PARAMETER["ip_address_type"] == resource.get("ip_address_type")
    assert compare_attributes(PARAMETER["attributes"], resource.get("attributes"))
    assert resource.get("targets") and len(resource.get("targets")) == len(
        PARAMETER["targets"]
    )
    assert compare_targets(PARAMETER["targets"], resource.get("targets"))


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get-by-resource_id", depends=["present"])
async def test_exec_get_by_resource_id(hub, ctx):
    # This test  is here to avoid creating another ElasticLoadBalancingv2 Target Group fixture for exec.get() testing.
    ret = await hub.exec.aws.elbv2.target_group.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["matcher"] == resource.get("matcher")
    assert compare_attributes(PARAMETER["attributes"], resource.get("attributes"))
    assert PARAMETER["health_check_path"] == resource.get("health_check_path")
    assert PARAMETER["health_check_enabled"] == resource.get("health_check_enabled")
    assert PARAMETER["target_type"] == resource.get("target_type")
    assert PARAMETER["ip_address_type"] == resource.get("ip_address_type")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get-by-name", depends=["present"])
async def test_exec_get_by_name(hub, ctx):
    ret = await hub.exec.aws.elbv2.target_group.get(
        ctx,
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert resource.get("tags")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["matcher"] == resource.get("matcher")
    assert compare_attributes(PARAMETER["attributes"], resource.get("attributes"))
    assert PARAMETER["target_type"] == resource.get("target_type")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update-target-group", depends=["present"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["tags"] = {"Name": PARAMETER["name"], "Description": "ELB Load Balancer."}
    PARAMETER["matcher"] = {
        "HttpCode": "300",
    }
    PARAMETER["protocol_version"] = "HTTP2"
    ret = await hub.states.aws.elbv2.target_group.present(
        ctx,
        name=PARAMETER["name"],
        protocol_version=PARAMETER["protocol_version"],
        matcher=PARAMETER["matcher"],
        tags=PARAMETER["tags"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )

    assert ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["matcher"] == resource.get("matcher")
    assert compare_attributes(PARAMETER["attributes"], resource.get("attributes"))


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update-tags", depends=["update-target-group"])
async def test_update_targets_and_tags(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["tags"] = {
        "Description": "ELB Load Balancer. Updated",
        "Summary": "Summary of ELB Load Balancer",
    }
    PARAMETER["matcher"] = {
        "HttpCode": "200",
    }
    ret = await hub.states.aws.elbv2.target_group.present(
        ctx,
        name=PARAMETER["name"],
        tags=PARAMETER["tags"],
        matcher=PARAMETER["matcher"],
        targets=PARAMETER["targets"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    resource = ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["tags"] == resource.get("tags")
    assert resource.get("targets") and len(resource.get("targets")) == len(
        PARAMETER["targets"]
    )
    assert compare_targets(PARAMETER["targets"], resource.get("targets"))
    assert compare_attributes(PARAMETER["attributes"], resource.get("attributes"))


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["update-tags"])
async def test_describe(hub, ctx):
    describe_response = await hub.states.aws.elbv2.target_group.describe(ctx)
    assert describe_response[PARAMETER["resource_id"]]
    assert describe_response.get(PARAMETER["resource_id"]) and describe_response.get(
        PARAMETER["resource_id"]
    ).get("aws.elbv2.target_group.present")
    described_resource = describe_response.get(PARAMETER["resource_id"]).get(
        "aws.elbv2.target_group.present"
    )
    resource = dict(ChainMap(*described_resource))
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["health_check_path"] == resource.get("health_check_path")
    assert PARAMETER["health_check_enabled"] == resource.get("health_check_enabled")
    assert PARAMETER["health_check_interval_seconds"] == resource.get(
        "health_check_interval_seconds"
    )
    assert PARAMETER["health_check_timeout_seconds"] == resource.get(
        "health_check_timeout_seconds"
    )
    assert PARAMETER["healthy_threshold_count"] == resource.get(
        "healthy_threshold_count"
    )
    assert PARAMETER["unhealthy_threshold_count"] == resource.get(
        "unhealthy_threshold_count"
    )
    assert PARAMETER["matcher"] == resource.get("matcher")
    assert PARAMETER["target_type"] == resource.get("target_type")
    assert PARAMETER["ip_address_type"] == resource.get("ip_address_type")
    assert resource.get("targets") and len(resource.get("targets")) == len(
        PARAMETER["targets"]
    )
    assert compare_targets(PARAMETER["targets"], resource.get("targets"))
    assert compare_attributes(PARAMETER["attributes"], resource.get("attributes"))


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.elbv2.target_group.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    describe_resource = ret.get("old_state")
    resource = dict(describe_resource)
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["health_check_path"] == resource.get("health_check_path")
    assert PARAMETER["health_check_enabled"] == resource.get("health_check_enabled")
    assert PARAMETER["health_check_interval_seconds"] == resource.get(
        "health_check_interval_seconds"
    )
    assert PARAMETER["health_check_timeout_seconds"] == resource.get(
        "health_check_timeout_seconds"
    )
    assert PARAMETER["healthy_threshold_count"] == resource.get(
        "healthy_threshold_count"
    )
    assert PARAMETER["unhealthy_threshold_count"] == resource.get(
        "unhealthy_threshold_count"
    )
    assert PARAMETER["matcher"] == resource.get("matcher")
    assert PARAMETER["target_type"] == resource.get("target_type")
    assert PARAMETER["ip_address_type"] == resource.get("ip_address_type")
    assert resource.get("targets") and len(resource.get("targets")) == len(
        PARAMETER["targets"]
    )
    assert compare_attributes(PARAMETER["attributes"], resource.get("attributes"))
    assert compare_targets(PARAMETER["targets"], resource.get("targets"))
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.elbv2.target_group.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_absent_with_none_resource_id(hub, ctx):
    ret = await hub.states.aws.elbv2.target_group.absent(ctx, name=PARAMETER["name"])
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_absent_with_invalid_resource_id(hub, ctx):
    ret = await hub.states.aws.elbv2.target_group.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id="invalid-arn",
    )
    assert not ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert (
        f"ClientError: An error occurred (ValidationError) when calling the DescribeTargetGroups operation: "
        f"'invalid-arn' is not a valid target group ARN" in str(ret["comment"])
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if hub.tool.utils.is_running_localstack(ctx):
        return
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.elbv2.target_group.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]


def compare_targets(
    in_targets: List[Dict[str, Any]], out_targets: List[Dict[str, Any]]
):
    target_map = {target.get("Id"): target for target in out_targets}
    for target in in_targets:
        if target.get("Id") in target_map:
            if target_map.get(target.get("Id")) != target:
                return False
        else:
            return False
    return True


def compare_attributes(
    input_attributes: List[Dict[str, Any]], output_attributes: List[Dict[str, Any]]
):
    assert output_attributes
    out_attributes_map = {
        attribute.get("Key"): attribute for attribute in output_attributes or []
    }

    for attribute in input_attributes:
        if attribute.get("Key") in out_attributes_map:
            if attribute != out_attributes_map.get(attribute.get("Key")):
                return False
    return True
