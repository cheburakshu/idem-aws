from unittest import mock

import pytest

profile = {
    "aws_access_key_id": "my_key_id",
    "aws_secret_access_key": "my_secret_key",
    "region": "us-east-1",
}


async def _my_boto3_exec(*args, **kwargs):
    return await _wrap(*args, **kwargs)


async def _wrap(*args, **kwargs):
    return {
        "UserId": "dummy_user_id",
        "Account": "dummy_account_id",
        "Arn": "dummy_arn",
    }


@pytest.mark.asyncio
async def test_gather(hub):
    hub.tool.boto3.client.exec = _my_boto3_exec

    with mock.patch("botocore.config.Config") as config:
        p1 = await hub.acct.metadata.aws.gather(profile=profile)
    assert p1 is not None
    assert p1["attribute_value"] == "dummy_account_id"
    assert p1["attribute_key"] == "Account"
    assert p1["type"] == "AWS"
